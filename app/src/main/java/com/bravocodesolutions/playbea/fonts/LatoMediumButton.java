package com.bravocodesolutions.playbea.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by shahana-amina on 6/10/15.
 */
public class LatoMediumButton extends Button {
    public LatoMediumButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    public LatoMediumButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public LatoMediumButton(Context context) {
        super(context);
    }
    public void setTypeface(Typeface tf, int style) {

        super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato_Medium.ttf"));

    }
}
