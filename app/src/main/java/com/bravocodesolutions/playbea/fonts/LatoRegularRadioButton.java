package com.bravocodesolutions.playbea.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;

/**
 * Created by shahana-amina on 5/10/15.
 */
public class LatoRegularRadioButton extends RadioButton {

    public LatoRegularRadioButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    public LatoRegularRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public LatoRegularRadioButton(Context context) {
        super(context);
    }
    public void setTypeface(Typeface tf, int style) {
        if (style == Typeface.BOLD) {
            super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato_Bold.ttf"));

        } else {
            super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato_Regular.ttf"));


        }
    }
}
