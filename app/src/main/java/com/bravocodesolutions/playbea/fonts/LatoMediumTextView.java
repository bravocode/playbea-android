package com.bravocodesolutions.playbea.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by shahana-amina on 4/10/15.
 */
public class LatoMediumTextView extends TextView {
    public LatoMediumTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    public LatoMediumTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public LatoMediumTextView(Context context) {
        super(context);
    }
    public void setTypeface(Typeface tf, int style) {

        super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato_Medium.ttf"));

    }
}
