package com.bravocodesolutions.playbea.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by shahana-amina on 3/10/15.
 */


public class LatoRegularTextView extends TextView {

    public LatoRegularTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    public LatoRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public LatoRegularTextView(Context context) {
        super(context);
    }
    public void setTypeface(Typeface tf, int style) {
        if (style == Typeface.BOLD) {
            super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato_Bold.ttf"));
        } else {
            super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato_Regular.ttf"));
        }
    }
}

