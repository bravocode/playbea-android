package com.bravocodesolutions.playbea.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.bravocodesolutions.playbea.R;

public class SignUpLoginActivity extends AppCompatActivity {

    Button signUpWithFbButton,loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_login);

        //Initializations
        signUpWithFbButton=(Button)findViewById(R.id.signUpWithFacebookButton);
        loginButton=(Button)findViewById(R.id.loginButton);


        signUpWithFbButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent signUpIntent=new Intent(SignUpLoginActivity.this,CreateYourAccountActivity.class);
                startActivity(signUpIntent);


            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent loginIntent=new Intent(SignUpLoginActivity.this,LoginActivity.class);
                startActivity(loginIntent);

            }
        });

    }
}
