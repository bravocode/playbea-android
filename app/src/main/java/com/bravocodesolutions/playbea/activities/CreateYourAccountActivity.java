package com.bravocodesolutions.playbea.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.bravocodesolutions.playbea.R;

public class CreateYourAccountActivity extends AppCompatActivity {
    Button createYourAccountButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_your_account);

        //Initializations
        createYourAccountButton=(Button)findViewById(R.id.createYourAccountButton);
        createYourAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }
}
