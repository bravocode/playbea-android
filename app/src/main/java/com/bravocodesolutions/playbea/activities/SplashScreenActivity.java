package com.bravocodesolutions.playbea.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.bravocodesolutions.playbea.R;

public class SplashScreenActivity extends AppCompatActivity {

    //Todo : Change Splash timeout on production
    private static int SPLASH_TIME_OUT = 3 * 1000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /** Hiding Title bar of this activity screen */
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        /** Making this activity, full screen */
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.activity_splash_screen);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                    Intent splashIntent = new Intent(SplashScreenActivity.this, SignUpLoginActivity.class);
                    startActivity(splashIntent);
                    finish();

            }
        }, SPLASH_TIME_OUT);

    }

}
