package com.bravocodesolutions.playbea;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WebView dummyWebview = (WebView)findViewById(R.id.webView);

        if (Build.VERSION.SDK_INT >= 19) {
            // chromium, enable hardware acceleration
            dummyWebview.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            // older android version, disable hardware acceleration
            dummyWebview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        dummyWebview.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        dummyWebview.getSettings().setJavaScriptEnabled(true);
        dummyWebview.setWebChromeClient(new WebChromeClient());
        dummyWebview.loadUrl("file:///android_asset/www/pages/index.html");
        dummyWebview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

    }
}
